#!/usr/bin/env python

import subprocess
import os
import glob
import configparser

#GOG2Proton

#TODO: MAKE SIMPLER VERSION FOR NON-GOG/ALREADY INSTALLED APPLICATIONS
#TODO: MAKE SIMPLER VERSION WITHOUT LIBRARY AND SHIT
#TODO: Add windows-safe option
#TODO: Create alternative to global vars
#TODO: Clear downloads feature

class Colours:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

def fancyText(bold, underline, colour, string):
    if bold:
        string = Colours.BOLD + string
    if underline:
        string = Colours.UNDERLINE + string
    if colour != None:
        string = colour + string
    return string + Colours.END

def readConfig():
    global downloadDir
    global installDir
    global protonDir
    global createDesktopFile
    global desktopFileIcon
    global convertIcon
    global createLocalScript
    global steamCompatible
    global getSteamBanner

    if os.path.isfile(home + "/.config/gog2proton/config.txt"):
        config = configparser.ConfigParser()
        config.read(home + "/.config/gog2proton/config.txt")
        downloadDir = config.get("DEFAULT", "download_dir")
        installDir = config.get("DEFAULT", "install_dir")
        protonDir = config.get("DEFAULT", "proton_dir")
        createDesktopFile = config.getboolean("DEFAULT", "desktop_file")
        desktopFileIcon = config.getboolean("DEFAULT", "add_icon")
        convertIcon = config.getboolean("DEFAULT", "convert_icon")
        createLocalScript = config.getboolean("DEFAULT", "local_script")
        steamCompatible = config.getboolean("DEFAULT", "steam_compatible")
        getSteamBanner = config.getboolean("DEFAULT", "get_steam_banner")
    else:
        #Set defaults
        downloadDir = home + "/Downloads"
        installDir = home + "/Games"
        createDesktopFile = True
        desktopFileIcon = True
        convertIcon = True
        createLocalScript = False
        steamCompatible = False
        getSteamBanner = False

        #Get proton directory and save new config
        setProtonDir()

def saveConfig():
    #Make config directory
    if os.path.isdir(home + "/.config/gog2proton") == False:
        os.makedirs(home + "/.config/gog2proton", 0o755)

    #Write new config file
    config = configparser.ConfigParser()
    config["DEFAULT"] = {
        "download_dir": downloadDir,
        "install_dir": installDir,
        "proton_dir": protonDir,
        "desktop_file": createDesktopFile,
        "add_icon": desktopFileIcon,
        "convert_icon": convertIcon,
        "local_script": createLocalScript,
        "steam_compatible": steamCompatible,
        "get_steam_banner": getSteamBanner
    }
    configFile = open(home + "/.config/gog2proton/config.txt", "w")
    config.write(configFile)
    configFile.close()

def addToLibrary(name, title, directory):
    library = configparser.ConfigParser()
    library.read(home + "/.config/gog2proton/library.txt")

    library[title] = {"name": name,
                      "game_dir": directory,
                      "desktop_file": createDesktopFile,
                      "local_script": createLocalScript}

    libraryFile = open(home + "/.config/gog2proton/library.txt", "w")
    library.write(libraryFile)
    libraryFile.close()

def printLibrary():
    library = configparser.ConfigParser()
    library.read(home + "/.config/gog2proton/library.txt")

    if len(library.sections()) == 0:
        print("No games are currently installed!")
    else:
        print(fancyText(True, False, None, "Installed games"))
        print("=========================")
        if len(library.sections()) == 1:
            print("1 game currently installed")
        else:
            print(str(len(library.sections())) + " games currently installed")
        print("-------------------------")

        for game in sorted(library.sections()):
            if game == "DEFAULT":
                continue
            print(game)
        print("=========================")

def installGame():
    #Get game name
    gameName = input("Type the game's name: ")
    gameName = gameName.lower()
    gameName = gameName.replace(" ", "_")

    #Get user game choice
    gameList = subprocess.check_output(["lgogdownloader", "--list", "--game", gameName]).decode()
    if gameList == "":
        print("No games found! Returning to menu.")
        return
    gameList = gameList.split("\n")
    gameList.pop()
    if len(gameList) == 1:
        gameName = gameList[0]
    else:
        print("Multiple games found!")
        gameNumber = 1
        for x in gameList:
            print(str(gameNumber) + ": " + x)
            gameNumber += 1
        gameChoice = input("Select the desired game: ")
        gameName = gameList[int(gameChoice)-1]

    #Get game's formatted title
    output = subprocess.check_output(["lgogdownloader", "--list-details", "--game", gameName]).decode()
    gameDetails = output.split("\n")
    gameTitle = gameDetails[2][7:]
    gameDir = installDir + '/' + gameTitle

    #Create download directory if necessary
    if os.path.isdir(downloadDir) == False:
        os.makedirs(downloadDir, 0o755)

    #Download installer
    subprocess.run(["lgogdownloader", "--download", "--directory", downloadDir, "--game", gameName + "$",
            "--platform", "w", "--include", "i"])

    #Create installation directory if necessary
    if os.path.isdir(installDir) == False:
        os.makedirs(installDir, 0o755)

    #Extract game files
    setupFile = glob.glob(downloadDir + '/' + gameName + "/*.exe")[0]
    subprocess.run(["innoextract", "-s", "-g", "--collisions", "overwrite",
            "-d", gameDir, setupFile])

    #Move files into base directory
    appFiles = glob.glob(gameDir + "/app/*")
    for x in appFiles:
        subprocess.run(["mv", "-n", x, gameDir])

    #Remove unnecessary directories
    subprocess.run(["rm", "-r", gameDir + "/app"])
    subprocess.run(["rm", "-r", gameDir + "/tmp"])
    subprocess.run(["rm", "-r", gameDir + "/sys"])
    subprocess.run(["rm", "-r", gameDir + "/__redist"])
    subprocess.run(["rm", "-r", gameDir + "/__support"])
    subprocess.run(["rm", "-r", gameDir + "/commonappdata"])

    #Create proton directory
    if os.path.isdir(gameDir + "/proton") == False:
        os.makedirs(gameDir + "/proton", 0o755)

    #Select main executable (if necessary)
    exeNumber = 1
    exeFiles = glob.glob(gameDir + "/*.exe")
    if len(exeFiles) > 1:
        print()
        for x in exeFiles:
            print(str(exeNumber) + ": " + x)
            exeNumber += 1
        exeChoice = input("Select the game's main executable: ")
        exePath = exeFiles[int(exeChoice)-1]
    else:
        exePath = exeFiles[0]

    #Create launch script
    launchScript = open(gameDir + "/launch.sh", "w")
    launchScript.write("#!/bin/sh\n\n")
    launchScript.write('cd "$(dirname "$(realpath "$0")")"\n')
    launchScript.write("STEAM_COMPAT_DATA_PATH='" + gameDir + "/proton' ")
    launchScript.write("'" + protonDir + "/proton' " + "waitforexitandrun '" + exePath + "'")
    launchScript.close()
    subprocess.run(["chmod", "+x", gameDir + "/launch.sh"])

    #Create local launch script
    if createLocalScript:
        if os.path.isdir(home + "/.local/bin") == False:
            os.makedirs(home + "/.local/bin", 0o755)

        launchScript = open(home + "/.local/bin/" + gameName, "w")
        launchScript.write("#!/bin/sh\n\n")
        launchScript.write("cd '" + gameDir + "'\n")
        launchScript.write("STEAM_COMPAT_DATA_PATH='" + gameDir + "/proton' ")
        launchScript.write("'" + protonDir + "/proton' " + "waitforexitandrun '" + exePath + "'")
        launchScript.close()
        subprocess.run(["chmod", "+x", home + "/.local/bin/" + gameName])

    #Retrieve steam banner
    if getSteamBanner:
        import requests
        import bs4
        import urllib.request

        req = requests.get("https://store.steampowered.com/search/?term=" + gameTitle)
        soup = bs4.BeautifulSoup(req.text, 'html.parser')

        for gameEntry in soup.find_all("a", "search_result_row ds_collapse_flag "):
            if gameTitle == gameEntry.find("span", "title").string:
                gameURL = gameEntry.get('href')
                gameID = gameURL.split('/')[4]
                urllib.request.urlretrieve("https://steamcdn-a.akamaihd.net/steam/apps/"
                        + gameID + "/header.jpg", gameDir + "/banner.jpg")

    #Create desktop file
    desktopFilePath = None
    if createDesktopFile:
        desktopFilePath = home + "/.local/share/applications/" + gameName + ".desktop"
        desktopFile = open(desktopFilePath, "w")
        desktopFile.write("[Desktop Entry]\n")
        desktopFile.write("Type=Application\n")
        desktopFile.write("Name=" + gameTitle + "\n")
        if steamCompatible:
            desktopFile.write(r"Exec=sh \'" + gameDir + r"/launch.sh\'" + "\n")
        else:
            desktopFile.write("Exec=sh '" + gameDir + "/launch.sh'\n")
        desktopFile.write("Terminal=false\n")
        desktopFile.write("Categories=Game\n")

        #Add icon entry
        if desktopFileIcon and not convertIcon:
            icon = glob.glob(gameDir + "/*.ico")
            desktopFile.write("Icon=" + icon[0])
        elif desktopFileIcon and convertIcon:
            icon = glob.glob(gameDir + "/*.ico")
            subprocess.run(["convert", icon[0], gameDir +"/icon.png"])
            desktopFile.write("Icon=" + gameDir + "/icon-5.png")

        desktopFile.close()

    #Add to library file
    addToLibrary(gameName, gameTitle, gameDir)

def deleteGame():
    library = configparser.ConfigParser()
    library.read(home + "/.config/gog2proton/library.txt")

    gameCount = 1

    if len(library.sections()) == 0:
        print("No games are currently installed!")
    else:
        print(fancyText(True, False, None, "Delete a game"))
        print("=========================")

        for game in sorted(library.sections()):
            if game == "DEFAULT":
                continue

            print(str(gameCount) + ". " + game)
            gameCount += 1
        print("=========================")
        gameChoice = input("Select a game: ")

        gameTitle = sorted(library.sections())[int(gameChoice)-1]
        gameName = library.get(gameTitle, "name")
        gameDir = library.get(gameTitle, "game_dir")
        desktopFile = library.getboolean(gameTitle, "desktop_file")
        localScript = library.getboolean(gameTitle, "local_script")

        subprocess.run(["rm", "-r", gameDir])

        if desktopFile:
            desktopFilePath = home + "/.local/share/applications/" + gameName + ".desktop"
            if os.path.isfile(desktopFilePath):
                os.remove(desktopFilePath)

        if localScript:
            localScriptPath = home + "/.local/bin/" + gameName
            if os.path.isfile(localScriptPath):
                os.remove(localScriptPath)

        library.remove_section(gameTitle)

        libraryFile = open(home + "/.config/gog2proton/library.txt", "w")
        library.write(libraryFile)
        libraryFile.close()

def setDownloadDir():
    global downloadDir

    print("Set download directory")
    print("======================")
    directory = input("Type your desired download directory: ")
    if os.path.isdir(directory):
        downloadDir = directory
        saveConfig()
    else:
        print(directory + " is not a directory or does not exist!")

def setInstallDir():
    global installDir

    print("Set install directory")
    print("=====================")
    directory = input("Type your desired install directory: ")
    if os.path.isdir(directory):
        installDir = directory
        saveConfig()
    else:
        print(directory + " is not a directory or does not exist!")

def setProtonDir():
    global protonDir

    #Check if library folder file exists
    protonDirs = []
    localLibraryDir = home + "/.local/share/Steam/steamapps"
    libraryFilePath = localLibraryDir + "/libraryfolders.vdf"
    libraries = [localLibraryDir + "/common"]
    if os.path.isfile(libraryFilePath):
        for line in open(libraryFilePath, "r"):
            line = line.lstrip()
            if line[1].isdigit():
                libraries.append(line.split('"')[3] + "/steamapps/common")
    else:
        print("Library folder file not found! Please check if steam is currently installed.")
        print("Aborting...")
        sys.exit(1)

    for library in libraries:
        for directory in os.listdir(library):
            if directory.startswith("Proton"):
                if os.path.isfile(library + "/" + directory + "/proton"):
                    protonDirs.append(library + "/" + directory)

    if len(protonDirs) == 1:
        print("Only one Proton version found!")
        print("Proton " + protonDirs[0].partition("Proton ")[2] + " automatically selected.")
        protonDir = protonDirs[0]
    elif len(protonDirs) > 1:
        dirNumber = 1
        print("Select Proton version")
        print("=====================")
        for directory in protonDirs:
            print(str(dirNumber) + ": " + directory)
        print("=====================")
        dirChoice = input("Select a Proton version: ")
        protonDir = protonDirs[int(dirChoice) - 1] + '/'
    else:
        print("No Proton directories found! Please download a game with Steam Play.")
        print("Aborting...")
        sys.exit(1)

    saveConfig()

def openOptions():
    global createDesktopFile
    global desktopFileIcon
    global convertIcon
    global createLocalScript
    global steamCompatible
    global getSteamBanner

    exit = False
    while exit == False:
        print()
        print(fancyText(True, False, None, "Options"))
        print("=========================")
        print("1. Create desktop file -- "
                + fancyText(True, False, None, str(createDesktopFile)))
        print("2. Add icon to desktop file -- "
                + fancyText(True, False, None, str(desktopFileIcon)))
        print("3. Convert icons to appropriate format -- "
                + fancyText(True, False, None, str(convertIcon)))
        print("4. Create launch script in ~/.local/bin -- "
                + fancyText(True, False, None, str(createLocalScript)))
        print("5. Steam compatible desktop files -- "
                + fancyText(True, False, None, str(steamCompatible)))
        print("6. Download steam banner after installation -- "
                + fancyText(True, False, None, str(getSteamBanner)))
        print("0. Return to main menu")
        print("=========================")
        menuChoice = input("Select an option: ")
        print()

        if menuChoice == "1":
            createDesktopFile = not createDesktopFile
            saveConfig()
        elif menuChoice == "2":
            desktopFileIcon = not desktopFileIcon
            saveConfig()
        elif menuChoice == "3":
            convertIcon = not convertIcon
            saveConfig()
        elif menuChoice == "4":
            createLocalScript = not createLocalScript
            saveConfig()
        elif menuChoice == "5":
            steamCompatible = not steamCompatible
            saveConfig()
        elif menuChoice == "6":
            getSteamBanner = not getSteamBanner
            saveConfig()
        elif menuChoice == "0":
            exit = True

def openMenu():
    quit = False
    while quit == False:
        print()
        print(fancyText(True, False, None, "GOG2Proton"))
        print("=========================")
        print("Download dir: " + downloadDir)
        print("Install dir: " + installDir)
        print("Proton ver: " + protonDir.partition("Proton ")[2])
        print("-------------------------")
        print("1. List GOG library")
        print("2. List installed games")
        print("3. Install a game")
        print("4. Delete a game")
        print("5. Set download directory")
        print("6. Set install directory")
        print("7. Select Proton version")
        print("8. Change options")
        print("0. Exit")
        print("=========================")
        menuChoice = input("Select an option: ")
        print()

        if menuChoice == "1":
            subprocess.run(["lgogdownloader", "--list"])
        elif menuChoice == "2":
            printLibrary()
        elif menuChoice == "3":
            installGame()
        elif menuChoice == "4":
            deleteGame()
        elif menuChoice == "5":
            setDownloadDir()
        elif menuChoice == "6":
            setInstallDir()
        elif menuChoice == "7":
            setProtonDir()
        elif menuChoice == "8":
            openOptions()
        elif menuChoice == "0":
            quit = True
            print("Bye!")

home = os.path.expanduser("~")
downloadDir = None
installDir = None
protonDir = None
createDesktopFile = None
desktopFileIcon = None
convertIcon = None
createLocalScript = None
steamCompatible = None
readConfig()
openMenu()
