# GOG2Proton

A Python script which allows you to download, install and manage GOG games and create launch scripts which utilise Valve Software's Proton.

### Features
* Configurable download and installation directories
* Delete installed games via script
* Proton directory detection and selection
* Create Steam-compatible desktop files
* Download game banner from Steam post-install

### Dependencies
* [Python 3](https://www.python.org/)
* [innoextract](https://github.com/dscharrer/innoextract/)
* [lgogdownloader](https://github.com/Sude-/lgogdownloader)

Optional:
* [ImageMagick](https://github.com/ImageMagick/ImageMagick) (for desktop file icon conversion)